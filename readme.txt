this folder contains the implementation of the algorithm proposed in the following paper:

"Formalizing Word Sampling for Vocabulary Prediction as Graph-based Active Learning" [Ehara et al., 2014]
http://emnlp2014.org/papers/pdf/EMNLP2014143.pdf

the folder contains the following files/folders:

active_learning.py --> given a graph as input, generates the most informative nodes of the graph. These nodes will be later used
to predict the labels of the other nodes in the  classification step
classifier.py --> given the labeled most informative nodes in a user vocabulary, predicts the labels of the remaining nodes of the vocabulary
label_propagation --> implements the label propagation algorithm used by the classifier

users_labels --> contains the dataset released by Ehara et al. [2010], containing 12,000 English words labeled by 15 Japanese learners of English. 
users_data  --> contains training and test data automatically generated for the classification step. label "1" means the user knows the word and label "0" means the user doesn't know the word.
classified_labels --> contains the output of the classifier, i.e. the labels for all 12,000 words assigned by the classifier