# coding=utf8
"""
Active Learning on Graphs via Generalization Error Bound Minimization
"""
import numpy as np
import networkx as nx
from scipy.sparse import lil_matrix
from scipy.linalg import block_diag
from scipy.sparse import block_diag as bd
from numpy import linalg as LA
from scipy.sparse import csgraph
import scipy.sparse as sparse
import os
import random
#np.seterr(divide='ignore', invalid='ignore')
from classifier import *

class ActiveLearning():
    '''
    Input: A = Adjacency matrix,
           l = number of nodes to select
           μ = regularization parameter
           e = small positive value to substitute the zero eigenvalues
           l = number of ndes to select
           n = order of each complete graph
    '''

    def __init__(self, m, l, n , num_cluster, e):
        self.m = m
        self.l = l
        self.n = n
        self.e = e
        self.A = None
        self.H = None
        self.H_k = None
        self.num_cluster = num_cluster
        self.class_acc = -1
        self.classifer = Classifier()
        self.classifer.build_initial_matrix(self.n)
        self.folder_user_labels = "users_labels"
        self.folder_user_train_data = "users_data/"

    #active learning algorithm: generalization error bound minimization
    def compute_generalization_error_bound_minimization(self):
        self.A = self.create_initial_adj_matrix()
        L = self.compute_normalized_laplacian_matrix(self.A)
        w, v = self.compute_eigen_decomposition(L)
        most_informative_nodes = self.compute_most_informative_nodes(w, v)
        return most_informative_nodes

    #computes the set of most informative nodes
    def compute_most_informative_nodes(self, w, v):
        self.H = self.compute_initial_H_matrix(w)
        #print "initial H matrix", self.H
        #initial empty set
        most_informative_nodes = set()

        i = 0
        count = 0
        while(len(most_informative_nodes) < self.l):

            most_informative_node = self.compute_most_informative_node(w, v, i, most_informative_nodes)
            most_informative_nodes.add(most_informative_node)

            #update H
            self.update_H_matrix(v, most_informative_node)
            i += 1
            count += 1

        self.build_class_training_set("", "", most_informative_nodes)
        self.class_acc = self.classifer.classify(len(most_informative_nodes))
        print ("-------------------------------------")
        #print ("accuracy --> ", self.class_acc)
        print "most informative nodes: ", most_informative_nodes
        print ("-------------------------------------")
        return most_informative_nodes

    #update H matrix
    def update_H_matrix(self, v, index):
        u_i = np.array(v[index, :])
        u_i = u_i.T
        #print u_i, self.H_k
        temp = np.dot(self.H_k, u_i)
        numerator = np.dot(self.H_k, u_i).T
        numerator = np.dot(temp, numerator)
        #print "numerator ", numerator
        denominator = np.dot(u_i.T, self.H_k)
        denominator = 1 + np.dot(denominator, u_i)
        #print "denominator ", denominator
        self.H_k = self.H_k - (numerator / denominator)

    #compute the most informative node from the seed pool
    def compute_most_informative_node(self, w,v, index, selected):
        least_selected_group = self.get_least_selected_group(selected)
        max_score = -1
        max_index = -1
        indexes = []
        scores = []
        for i in range (0, len(self.A)):
            u_i = np.array(v[i,:])
            u_i = u_i.T
            #print u_i
            if index == 0:
                self.H_k = np.array((1/self.H))
                self.H_k[self.H_k == np.inf] = 0

            temp = np.dot( self.H_k,  u_i)
            D = np.diag(w)
            D = 1/D
            D[D == np.inf] = 0
            #print "D ",D
            numerator = np.dot(temp.T, D)
            numerator = np.dot(numerator, temp)
            #print "numerator ", numerator
            denominator = np.dot(u_i.T, self.H_k )
            denominator = 1 + np.dot(denominator, u_i)
            #print "denominator ", denominator
            score = numerator/denominator
            #print "score of node ", i, ":", score
            indexes.append(i)
            scores.append(score)
            #print "i ==> ", i
            if score > max_score and i not in selected and self.is_least_selected_group(i, least_selected_group):
                max_score = score
                max_index = i
        #print "------------------------------------"
        #print "selected node, score: ", max_index, max_score
        #print "------------------------------------"

        max_indexes = []
        for index in indexes:
            #print "score ", scores[index], max_score
            if scores[index] == max_score:
                max_indexes.append(index)
        #print "max indexes ", len(max_indexes), max_indexes, max_score
        random.shuffle(max_indexes)
        return max_indexes[0]

    def get_least_selected_group(self, selected):
        groups = {}
        for i in range (0, self.num_cluster):
            groups[i] = 0

        groups = self.compute_node_group(selected, groups)

        ranked_values = sorted(groups.keys(), key=groups.__getitem__)
        ranked = sorted([(value, key) for (key, value) in groups.items()])
        least_selected_groups = []
        min = ranked[0][0]
        for v, k in ranked:
            if v == min:
                least_selected_groups.append(k)

        random.shuffle(least_selected_groups)
        #print "least_selected_groups ",least_selected_groups, min, ranked_values, ranked
        return least_selected_groups[0]

    def compute_node_group(self, nodes , groups):
        for node in nodes:
            group = node / self.n
            groups[group] += 1
        return groups

    def is_least_selected_group(self, index, least_selected_group):
        group = self.get_group(index)
        if group ==  least_selected_group:
            return True
        return False

    def get_group(self, node):
        return node/self.n

    #create initial adjacency matrix from corpus frequency list
    def create_initial_adj_matrix(self):
        G = nx.complete_graph(self.n)
        #print bd((nx.to_numpy_matrix(G), nx.to_numpy_matrix(G), nx.to_numpy_matrix(G))).toarray()
        M = bd((nx.to_scipy_sparse_matrix(G), nx.to_scipy_sparse_matrix(G), nx.to_scipy_sparse_matrix(G), nx.to_scipy_sparse_matrix(G),
                nx.to_scipy_sparse_matrix(G), nx.to_scipy_sparse_matrix(G), nx.to_scipy_sparse_matrix(G), nx.to_scipy_sparse_matrix(G),
               nx.to_scipy_sparse_matrix(G), nx.to_scipy_sparse_matrix(G), nx.to_scipy_sparse_matrix(G),
               nx.to_scipy_sparse_matrix(G))
               ).toarray()
        #M= np.matrix(bd((nx.to_numpy_matrix(G), nx.to_numpy_matrix(G), nx.to_numpy_matrix(G))))
        #print "M, ",M
        return M

    #computes the normalized laplacian matrix
    def compute_normalized_laplacian_matrix(self, M):
        #G = nx.from_numpy_matrix(M)
        #L = nx.normalized_laplacian_matrix(G)
        I = np.identity(self.num_cluster*self.n)
        L = csgraph.laplacian(M, normed=True)
        L = I -L
        #print "L ",L
        return L

    #performs eigen decomposition
    def compute_eigen_decomposition(self, L):
        #w, v = LA.eig(nx.to_numpy_matrix(nx.Graph(L)))
        w, v = sparse.linalg.eigsh(L)
        #print "w, v ", w, v
        w[w == 0] += self.e
        #print "w, v ", w,v
        return w, v

    #computes H0
    def compute_initial_H_matrix(self, w):
        h = 1 / (((self.m * w + 1) * (self.m * w + 1)) - 1)
        h[h == np.inf] = 0
        h[np.isnan(h)] = 0
        #print "h, ",h
        H_0 = np.diag(h)
        #print "H_0 ", H_0
        return H_0

    def build_class_training_set(self, input_folder, output_folder, most_info):
        folder =self.folder_user_labels
        file_count = 0
        for file in os.listdir(folder):
            if file.endswith(".txt"):
                f = open(os.path.join(folder, file))
                f_out = open(self.folder_user_train_data + str(
                    file_count) + ".txt", "w")
                ids = []
                labels = []
                words = []
                index = 0
                for line in f:
                    data = line.strip().split('\t')
                    # index = data[0]
                    label = data[1]
                    ids.append(str(index))
                    labels.append(label)
                    words.append(data[3])
                    index += 1

                for w in most_info:
                    label = labels[int(w)]
                    word = words[int(w)]
                    f_out.write(str(w) + " " + label + " " + label+ " " + word)
                    f_out.write('\n')

                for w in ids:
                    if int(w) not in most_info:
                        label = labels[int(w)]
                        word = words[int(w)]
                        f_out.write(w + " " + str(-1) + " " + label+ " " + word)
                        f_out.write('\n')
                    #else:
                    #    f_out.write(str(w) + " " + label + " " + label)
                    #    f_out.write('\n')
                f_out.close()
                file_count += 1
                #print f_out

if __name__ == '__main__':
    #m = 0.01 --> regularization parameter
    #l = 50 --> number of nodes to select
    #n = 1000 --> order of each complete graph
    #num_cluster = 12 --> number of clusters
    # e =  small positive value to substitute the zero eigen values

    al = ActiveLearning(0.01, 50,  1000, 12, 0.000001)
    #al.create_initial_matrix(9)
    acc_sum = 0
    num_runs = 10
    max_acc = -1
    max_info_nodes = []
    for i in range(0, num_runs):
        print "run number: ",i
        most_info = al.compute_generalization_error_bound_minimization()
        acc_sum += al.class_acc
        #print "-------------------------------"
        #print "run number: ", i
        #print "-------------------------------"
        if al.class_acc > max_acc:
            max_acc = al.class_acc
            max_info_nodes = most_info
        #break
    print "average accuracy ==> ", float(acc_sum)/float(num_runs)
    print "-------------------------------"
